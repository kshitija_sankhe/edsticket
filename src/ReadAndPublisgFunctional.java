import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.json.JSONArray;
import com.games24x7.framework.json.JSONException;
import com.games24x7.framework.json.JSONObject;
import com.games24x7.framework.mq.MQFrameworkFactory;

public class ReadAndPublisgFunctional
{

	static String QUEUE_NAME = "nfsticket";
	static String propfilepath = "C:/Users/Kshitija/workspace/EDsMQ/res/mq.properties";
	static PropsFileBasedConfiguration configvariable;
	static ArrayList<Long> userids = new ArrayList<Long>();
	
	public static void main( String args[] ) throws BiffException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, JSONException, ParseException
	{

		System.out.println( "in publisher" );
		configvariable = new PropsFileBasedConfiguration( propfilepath );
		MQFrameworkFactory.init( configvariable );
		MQFrameworkFactory.getFramework().registerQueuePublisher( QUEUE_NAME );
		
		JSONObject messageObject2=new JSONObject();
		
		

		// ******************excel
		String FilePath = "C:/Users/Kshitija/workspace/EDsMQ/sample.xls";
		FileInputStream fs = new FileInputStream( FilePath );
		Workbook wb = Workbook.getWorkbook( fs );// creating workbook
								
		Sheet sh = wb.getSheet( "Sheet1" );
		// number of rows present in the sheet
		int totalNoOfRows = sh.getRows();
		
		// To get the number of columns present in sheet
		int totalNoOfCols = sh.getColumns();
		System.out.println( "No of rows " + totalNoOfRows );
		
		SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yy HH:mm a");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		for( int row = 1; row < totalNoOfRows; row++ )
		{

			JSONObject jobject = new JSONObject();
			JSONObject json1=new JSONObject();
			JSONArray jArray=new JSONArray();
			
			/*Date date = format1.parse( sh.getCell( 5, row ).getContents());
			format.format( date );*/
			
			int ticket=Integer.parseInt( sh.getCell( 2, row ).getContents() );
			
			jobject.put( "source", sh.getCell( 0, row ).getContents() );
			jobject.put( "dest", sh.getCell( 1, row ).getContents() );
			jobject.put( "ticketId", Long.parseLong( sh.getCell( 2, row ).getContents() ) );
			jobject.put( "ticketName", sh.getCell( 3, row ).getContents() );
			jobject.put( "txnId", Integer.parseInt( sh.getCell( 4, row ).getContents() ) );
			jobject.put(  "startDate",format.format(format1.parse( sh.getCell( 5, row ).getContents())) );
			jobject.put( "endDate", format.format(format1.parse( sh.getCell( 6, row ).getContents()))  );
			jobject.put( "noOfTickets", Integer.parseInt( sh.getCell( 7, row ).getContents() ));
			jobject.put( "userRole", Integer.parseInt( sh.getCell( 8, row ).getContents() ));
			
			
			String uid=sh.getCell( 9, row ).getContents();
			
			 String[] dataArray = uid.split(",");
			   for (String item:dataArray) { 
				   jArray.put(Long.parseLong(item));
				   userids.add(Long.parseLong( item ));
			   }
			jobject.put("userIds",jArray);
			jobject.put("ticketType", sh.getCell( 10, row ).getContents() );
			jobject.put( "comments", sh.getCell( 11, row ).getContents() );
			
			
			
			json1.put( "eventType","ticketAssign" );
			json1.put( "value",jobject.toString() );
			System.out.println("final message  "+json1.toString());
			MQFrameworkFactory.getFramework().publishToQueue( QUEUE_NAME, json1.toString() );
			
			//***************************************For DB- call to dbconnect method	
				
			DBconnectionCode dbconObject=new DBconnectionCode();
			dbconObject.DbResult( userids, ticket );
		}
		
		
		
		 

		
		
		
	}

}