import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;



import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.json.JSONArray;
import com.games24x7.framework.json.JSONException;
import com.games24x7.framework.json.JSONObject;
import com.games24x7.framework.mq.MQFrameworkFactory;

public class ReadAndPublish
{

	static String QUEUE_NAME = "nfsticket";
	static String propfilepath = "/home/kshitija/edsticket/edsticket/res/mq.properties";
	static PropsFileBasedConfiguration configvariable;
	static ArrayList<Long> userids = new ArrayList<Long>();
	
	public static void main( String args[] ) throws  IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, JSONException, ParseException
	{

		
		configvariable = new PropsFileBasedConfiguration( propfilepath );
		MQFrameworkFactory.init( configvariable );
		MQFrameworkFactory.getFramework().registerQueuePublisher( QUEUE_NAME );
		
		JSONObject messageObject2=new JSONObject();
		
			JSONObject jobject = new JSONObject();
			JSONObject json1=new JSONObject();
			JSONArray jArray=new JSONArray();
			
			
			jobject.put( "source", configvariable.getStringValue("source.data") );
			jobject.put( "dest", configvariable.getStringValue("dest.data") );
			jobject.put( "ticketId", configvariable.getLongValue("ticketId.data"));
			jobject.put( "ticketName", configvariable.getStringValue("ticketName.data")  );
			jobject.put( "txnId", configvariable.getIntValue("txnid.data") );
			jobject.put(  "startDate",configvariable.getStringValue("startTime.data") );
			jobject.put( "endDate", configvariable.getStringValue("endTime.data")  );
			jobject.put( "noOfTickets", configvariable.getIntValue("noOfTickets.data"));
			
			jobject.put( "userRole",  configvariable.getIntValue("userRole"));
			jArray.put(configvariable.getLongValue("userid.data"));
			jArray.put(configvariable.getLongValue("userid2.data"));
			//jobject.put("userIds",jArray);
			jobject.putOnce("userIds", null);   //to test userid=null test case
			
			jobject.put("ticketType",configvariable.getStringValue("ticketType.data") );
			jobject.put( "comments", configvariable.getStringValue("comments.data"));
			System.out.println("jarray " +jobject.toString());
			
			
			json1.put( "eventType","ticketAssign" );
			json1.put( "value",jobject.toString() );
			System.out.println("final message  "+json1.toString());
			MQFrameworkFactory.getFramework().publishToQueue( QUEUE_NAME, json1.toString() );
			
			/*//***************************************For DB- call to dbconnect method	
				
			DBconnectionCode dbconObject=new DBconnectionCode();
			dbconObject.DbResult( userids, ticket );*/
		}
		
		
		
		 

		
		
		
	}


