import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.mq.MQFrameworkFactory;


public class Consumer {
	static String QUEUE_NAME="nfsticket";
	static String propfilepath="C:/Users/Kshitija/workspace/EDsMQ/res/mq.properties";
	private static  PropsFileBasedConfiguration configvariable;
	public static void main(String args[]){
		

		 configvariable=new PropsFileBasedConfiguration(propfilepath);
		
		
		   MQFrameworkFactory.init(configvariable);
		
		  
		  MQFrameworkFactory.getFramework().registerQueueConsumer(QUEUE_NAME,  new ConsumerHandler());
		  
		  System.out.println("Consumer initialized");
		
	}

}
